import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


function Square(props) {
    return (
        <button className={props.classes} onClick={props.onClick}>
            {props.value}
        </button>
    );
}

class Board extends React.Component {
    renderSquare(i) {
        let style = this.props.winners.indexOf(i) > -1 ? "square winner" : "square"
        return (
            <Square key={i} value={this.props.squares[i]}
                onClick={() => this.props.onClick(i)} classes={style} />
        );
    }
    montarBoard() {
        let indice = 0;
        let board = new Array(3);
        for(let i=0; i<3; i++){
            let linha = new Array(3);   
            for(let j=0; j<3; j++){
                let square = this.renderSquare(indice);
                linha[j] = square;
                indice++;
            }
            let boardRow = <div className="board-row" key={i}>{linha}</div>;
            board[i] = boardRow;
        }
        return( board );
    }
    render() {
        const board = this.montarBoard();
        return ( <div>{ board }</div> );
    }
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null),
                local: [],
            }],
            stepNumber: 0,
            xIsNext: true,
            ordemCrescente: true,
        };
    }
    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        const local = current.local.slice();
        if (calculateWinner(squares) || squares[i]) {
            return;
        }
        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([{
                squares: squares,
                local: local.concat(i),
            }]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext,
        });
    }
    jumpTo(step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0,
        });
    }
    inverterOrdem(){
        this.setState({
            ordemCrescente: !this.state.ordemCrescente,
        });
    }
    coordenadas(indice) {
        const linha = indice % 3 + 1;
        const coluna = parseInt(indice / 3) + 1;
        return (" (" + coluna + ", " + linha + ")");
    }
    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner = calculateWinner(current.squares);
        let moves = history.map((step, move) => {
            let desc = move ?
                'Go to move #' + move :
                'Go to game start';

            const coordenadas = move ? this.coordenadas(step.local[step.local.length - 1]) : "";

            if (move === this.state.stepNumber) {
                desc = <b>{desc}</b>
            }

            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{desc}</button>{coordenadas}
                </li>
            );
        });
        let status;
        let winners = [];
        if (winner) {
            status = 'Winner: ' + winner[0];
            winners = winner[1];
        } else if (this.state.stepNumber < 9){
            status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        } else {
            status = 'DRAW!';
            winners = [0,1,2,3,4,5,6,7,8];
        }
        
        moves = this.state.ordemCrescente ? moves : moves.reverse();
        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        winners = {winners}
                        onClick={(i) => this.handleClick(i)}
                    />
                </div>
                <div className="game-info">
                    <div><b>{status}</b></div><br/>
                    <button onClick={() => this.inverterOrdem()}>Inverter ordem dos movimentos</button>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [2, 4, 6],
        [0, 4, 8],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return [squares[a], [a,b,c]];
        }
    }
    return null;
}
// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
